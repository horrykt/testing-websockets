FROM anapsix/alpine-java:8u172b11_server-jre

MAINTAINER Bogdan Draghicescu <bogdan.draghicescu@whitebox.tech>

#configure constants 
ENV APP_INSTALL_DIR /opt/app/

RUN mkdir -p ${APP_INSTALL_DIR}

WORKDIR ${APP_INSTALL_DIR}
ADD "build/libs/ws-app*.jar" ${APP_INSTALL_DIR}/app.jar

EXPOSE 3000

ENTRYPOINT ["java", "-jar", "app.jar"]