package hello.controller;

import hello.dto.MessageRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HttpRequestController {
    @Autowired
    private SimpMessagingTemplate messagingTemplate;


    @PostMapping("/user/msg")
    public void sendTargetedGreeting(@RequestBody MessageRequest request) {
        System.out.println("Sending notif to user: " + request.getDestinationUsername());
        messagingTemplate.convertAndSendToUser(request.getDestinationUsername(), "/queue/reply", request.getMessage());
    }
}
