package hello.controller;

import hello.Application;
import hello.dto.MessageRequest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.validator.ValidateWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.ApplicationContext;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.boot.test.*;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import javax.servlet.ServletContext;
import java.lang.reflect.Type;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeoutException;

import static java.util.Arrays.asList;
import static java.util.concurrent.TimeUnit.SECONDS;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class HttpRequestControllerTest {

    @LocalServerPort
    private int port;



//    @Value("server.servlet.context-path")
//    private String contextPath=System.getProperty("server.servlet.context-path");

    @Autowired
    ServletContext servletContext;

    String WEBSOCKET_URI;
    final String WEBSOCKET_TOPIC = "/user/queue/reply/";

    BlockingQueue<String> blockingQueue;
    WebSocketStompClient stompClient;

    @Autowired
    HttpRequestController httpRequestController;

    @Before
    public void setup() {
        WEBSOCKET_URI = "ws://127.0.0.1:" + port + servletContext.getContextPath() +"/gs-guide-websocket/";
        blockingQueue = new LinkedBlockingDeque<>();
        stompClient = new WebSocketStompClient(
                //              new SockJsClient(
                //              asList(
                //                     new WebSocketTransport(
                new StandardWebSocketClient()
                //                    )))
        );
    }


    @Test
    public void sendTargetedGreeting() throws InterruptedException, ExecutionException, TimeoutException {
//        WebSocketStompClient stompClient = new WebSocketStompClient(new SockJsClient(createTransportClient()));
//        stompClient.setMessageConverter(new MappingJackson2MessageConverter());

        StompSession session = stompClient
                .connect(WEBSOCKET_URI, new StompSessionHandlerAdapter() {
                })
                .get(10, SECONDS);
        session.subscribe(WEBSOCKET_TOPIC, new DefaultStompFrameHandler());
        MessageRequest request = new MessageRequest();
        request.setDestinationUsername("user");
        request.setMessage("wow");
        session.send(request.getDestinationUsername(), request.getMessage());
        //   httpRequestController.sendTargetedGreeting(request);
        Assert.assertEquals(request.getMessage(), blockingQueue.poll(10, SECONDS));

    }

    class DefaultStompFrameHandler implements StompFrameHandler {
        @Override
        public Type getPayloadType(StompHeaders stompHeaders) {
            return byte[].class;
        }

        @Override
        public void handleFrame(StompHeaders stompHeaders, Object o) {
            blockingQueue.offer(new String((byte[]) o));
        }
    }

}